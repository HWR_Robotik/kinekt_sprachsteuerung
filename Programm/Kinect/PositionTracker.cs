﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;

namespace BotAtHWR
{
    class PositionTracker
    {
        private static double _recentPos;
        private static double _defaultDistance;
        private static Boolean _isRobbieDriving;


        public PositionTracker()
        {
            _recentPos = 0;

            //Distanz welche der Roboter zum Benutzer einhalten soll
            //vllt ein bisschen mit der Zahl herumspielen
            _defaultDistance = 1200;
            
        }

        public static void trackPos(ref Skeleton me, DepthImageFrame depthFrame)
        {
            DepthImagePoint _type1DepthPoint = depthFrame.MapFromSkeletonPoint(me.Joints[JointType.HipCenter].Position);
            
            //depth ist die aktuelle Position des Benutzers
            double depth = _type1DepthPoint.Depth;

            //defaultOffset beschreibt die positionsänderung zur DefaultDist
            double defaultOffset = depth - _defaultDistance;

            //wäre der Buffer auf 0 so würde der Roboter sofort losfahren bei der geringsten Änderung
            double trackingBuffer = 40;

            
            if(defaultOffset <= trackingBuffer)
            {
                if(_isRobbieDriving)
                {
                    _isRobbieDriving = false;    

                    /*Event e = new Event();
                    e.sender = "kinect";
                    e.time = DateTime.Now;
                    e.message = "StopDriving";
                    _client.SendEvent(e);*/
                }

                return;
            }
            else if(defaultOffset > trackingBuffer)
            {
                _isRobbieDriving = true;

                /*Event e = new Event();
                e.sender = "kinect";
                e.time = DateTime.Now;
                e.message = "StartDriving";
                _client.SendEvent(e);*/
            }
        }
    }
}
