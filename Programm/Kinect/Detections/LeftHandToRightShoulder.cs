﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BotAtHWR;
using Microsoft.Kinect;

namespace kinect.Detections
{
    class LeftHandToRightShoulder : ADetection
    {

        private long _recentPos;
        private Engine _engine;
        
        public LeftHandToRightShoulder(Engine e)
        {
            _type1 = JointType.HandLeft;
            _type2 = JointType.ShoulderRight;
            _engine = e;
        }

        public override void startSomething()
        {

            if (!_wasDetected)
            {

                if (!nur_following)
                {
                    //send something to server
                    /*Event e = new Event();
                    e.sender = "kinect";
                    e.time = DateTime.Now;
                    e.message = "LeftHandToRightShoulder";
                    _client.SendEvent(e);*/
                    Console.WriteLine("Send Event LeftHandToRightShoulder");
                }
            }
        }

        public override void stopSomething()
        {
            if (_wasDetected)
            {
                //send something to server
            }

        }
    }
}
