﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BotAtHWR;
using Microsoft.Kinect;

namespace kinect.Detections
{
    class RightHandToRightShoulder : ADetection
        
    {
        private Engine _engine;
        public RightHandToRightShoulder(Engine e)
        {
            _type1 = JointType.HandRight;
            _type2 = JointType.ShoulderRight;
            _engine = e;
        }

        public override void startSomething()
        {

            if (!_wasDetected)
            {

                if (!nur_following)
                {
                    //send something to server
                    /*Event e = new Event();
                    e.sender = "kinect";
                    e.time = DateTime.Now;
                    e.message = "RightHandToRightShoulder";
                    _client.SendEvent(e);*/
                    _engine.Move(-1000000, -1000000);
                    Console.WriteLine("Send Event RightHandToRightShoulder");
                }
            }
        }

        public override void stopSomething()
        {
            if (_wasDetected)
            {
                //send something to server
            }

        }
    }
}
