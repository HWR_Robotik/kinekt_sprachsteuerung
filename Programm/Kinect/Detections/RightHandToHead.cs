﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BotAtHWR;
using Microsoft.Kinect;

namespace kinect.Detections
{
    class RightHandToHead : ADetection
    {

        private const int _skeletonCount = 6;
        private Skeleton[] allSkeletons = new Skeleton[_skeletonCount];
        private KinectSensor _sensor;
        private Engine _engine;
        float old = 0f;
        float grad = 0f;

        public RightHandToHead(Engine e,KinectSensor sensor)
        {
            _type1 = JointType.HandRight;
            _type2 = JointType.Head;
            _engine = e;
            _sensor = sensor;
        }

        public override void startSomething()
        {

            if (!_wasDetected)
            {
                //send something to server
                /*Event e = new Event();
                e.sender = "kinect";
                e.time = DateTime.Now;
                e.message = "RightHandToHead";
                _client.SendEvent(e);*/
                Console.WriteLine("Send Event RightHandToHead- following mode ");
                //Thread thread = new Thread(startUpKinect_s);
                nur_following = !nur_following;
                if (nur_following)
                {
                    Thread thread = new Thread(startUpKinect_s);
                    thread.Start();
                }
            }
        }


        private void startUpKinect_s()
        {
            if (KinectSensor.KinectSensors.Count > 0)
            {
                _sensor = KinectSensor.KinectSensors[0];
            }

            if (_sensor.Status == KinectStatus.Connected)
            {
                _sensor.ColorStream.Enable();
                _sensor.DepthStream.Enable();
                _sensor.SkeletonStream.Enable();

                //only for Kinect for Microsoft version
                //sensor.DepthStream.Range = DepthRange.Near;
                //sensor.SkeletonStream.EnableTrackingInNearRange = true;
                //sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated
                _sensor.AllFramesReady += following;
                _sensor.Start();
            }
        }


        public void getSkeleton(AllFramesReadyEventArgs e, ref Skeleton me)
        {
            using (SkeletonFrame skeletonData = e.OpenSkeletonFrame())
            {
                if (skeletonData == null)
                {
                    return;
                }

                skeletonData.CopySkeletonDataTo(allSkeletons);

                me = (from s in allSkeletons where s.TrackingState == SkeletonTrackingState.Tracked select s).FirstOrDefault();

            }
        }

        /*      private void following(object sender, AllFramesReadyEventArgs e)
              {
                  Skeleton me = null;

                  getSkeleton(e, ref me);
                  Joint bauch = me.Joints[JointType.HipCenter];
                  Console.WriteLine("z:" + bauch.Position.Z.ToString());

                  // Joint bauch = me.Joints[JointType.HipCenter];
                  float old = 0f;
                  float distanz;
                  while (true)
                  {
                      getSkeleton(e, ref me);
                      if (me == null)
                      {
                          return;
                      }
                       bauch = me.Joints[JointType.HipCenter];
                      Console.WriteLine("z:" + bauch.Position.Z.ToString());
                      distanz = bauch.Position.Z - old; // hier noch cm umrechnung
                      _engine.MoveInCm((int) distanz);
                      old = bauch.Position.Z;
                  }
              }
              */
        private void following(object sender, AllFramesReadyEventArgs e)
        {
            if (nur_following)
            {
                Skeleton me = null;
                getSkeleton(e, ref me);
                Joint bauch = me.Joints[JointType.HipCenter];
                //if (old != 0)
                if (old <= -2 || old >= 2 || grad <= -2 || grad >= 2)
                {
                    if (me == null)
                    {
                        return;
                    }
                    //drehen
                    float drehen = (float)((bauch.Position.X - grad) * 10); // hier noch cm umrechnung
                                                                            //_engine.TurnInDegrees(-(int)drehen);
                    _engine.TurnInDegrees(-(int)drehen);
                    // old --> Distanz
                    float distanz = (float)(bauch.Position.Z - old) * 120; // hier noch cm umrechnung
                    Console.WriteLine("distanz:" + distanz.ToString());
                    _engine.MoveInCm(-(int)distanz);
                }
                if (old == 0)
                {
                    old = bauch.Position.Z;
                    grad = bauch.Position.X;
                }
            }
            //            detectGesture(e, ref me);
        }


        public override void stopSomething()
        {
            if (_wasDetected)
            {
                //send something to server
            }

        }
    }
}
