﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BotAtHWR;
using Microsoft.Kinect;

namespace kinect.Detections
{
    class LeftHandToHipCenter : ADetection
    {
        private Engine _engine;
        public LeftHandToHipCenter(Engine e)
        {
            _type1 = JointType.HandLeft;
            _type2 = JointType.HipCenter;
            _engine = e;
        }

        public override void startSomething()
        {

            if (!_wasDetected)
            {

                if (!nur_following)
                {
                    //send something to server
                    /*Event e = new Event();
                    e.sender = "kinect";
                    e.time = DateTime.Now;
                    e.message = "LeftHandToStomache";
                    _client.SendEvent(e);*/
                    Console.WriteLine("Send Event LeftHandToStomache - STOP");
                    _engine.StopImmediately();
                }
                //_engine.StopWheels();
                }
        }

        public override void stopSomething()
        {
            if (_wasDetected)
            {
                //send something to server
            }

        }
    }
}
