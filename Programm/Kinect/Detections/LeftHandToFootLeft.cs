﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BotAtHWR;
using Microsoft.Kinect;

namespace kinect.Detections
{
    class LeftHandToFootLeft : ADetection
    {

        private long _recentPos;
        private Engine _engine;
        
        public LeftHandToFootLeft(Engine e)
        {
            _type1 = JointType.HandLeft;
            _type2 = JointType.FootLeft;
            _engine = e;
        }

        public override void startSomething()
        {

            if (!_wasDetected)
            {
                if (!nur_following) { 
                //send something to server
                /*Event e = new Event();
                e.sender = "kinect";
                e.time = DateTime.Now;
                e.message = "LeftHandToFootLeft";
                _client.SendEvent(e);*/
                Console.WriteLine("Send Event LeftHandToFootLeft");
                }
            }
        }

        public override void stopSomething()
        {
            if (_wasDetected)
            {
                //send something to server
            }

        }
    }
}
