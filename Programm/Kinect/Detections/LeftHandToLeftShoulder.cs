﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BotAtHWR;
using Microsoft.Kinect;

namespace kinect.Detections
{
    class LeftHandToLeftShoulder : ADetection
    {

        private long _recentPos;
        private Engine _engine;
        public LeftHandToLeftShoulder(Engine e)
        {
            _type1 = JointType.HandLeft;
            _type2 = JointType.ShoulderLeft;
            _engine = e;
        }

        public override void startSomething()
        {

            if (!_wasDetected)
            {
                if (!nur_following)
                {
                    //send something to server
                    /*Event e = new Event();
                    e.sender = "kinect";
                    e.time = DateTime.Now;
                    e.message = "LeftHandToLeftShoulder";
                    _client.SendEvent(e);*/
                    _engine.Move(1000000,1000000);

                    Console.WriteLine("Send Event LeftHandToLeftShoulder");
                }
            }
        }

        public override void stopSomething()
        {
            if (_wasDetected)
            {
                //send something to server
            }

        }
    }
}
