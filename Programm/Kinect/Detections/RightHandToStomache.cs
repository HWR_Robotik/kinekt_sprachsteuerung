﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BotAtHWR;
using Microsoft.Kinect;

namespace kinect.Detections
{
    class RightHandToHipCenter : ADetection
    {
        private Engine _engine;

        public RightHandToHipCenter(Engine e)
        {
            _type1 = JointType.HandRight;
            _type2 = JointType.HipCenter;
            _engine = e;
        }

        public override void startSomething()
        {

            if (!_wasDetected)
            {

                if (!nur_following)
                {
                    //send something to server
                    /*Event e = new Event();
                    e.sender = "kinect";
                    e.time = DateTime.Now;
                    e.message = "RightHandToHipCenter";
                    _client.SendEvent(e);*/
                    Console.WriteLine("Send Event RightHandToHipCenter - STOP");
                    _engine.StopImmediately();
                }
                }
        }


        public override void stopSomething()
        {
            if (_wasDetected)
            {
                //send something to server
            }

        }
    }
}
