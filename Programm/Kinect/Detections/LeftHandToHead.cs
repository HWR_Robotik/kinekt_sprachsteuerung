﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BotAtHWR;
using Microsoft.Kinect;

namespace kinect.Detections
{
    class LeftHandToHead : ADetection
    {

        private long _recentPos;
        private Engine _engine;
        private KinectSensor _sensor;
        private Skeleton[] skeleton = new Skeleton[6];
        private Skeleton _onliThis;

        public LeftHandToHead(Engine e, KinectSensor sensor, Skeleton onliThis)
        {
            _type1 = JointType.HandLeft;
            _type2 = JointType.Head;
            _engine = e;
            _sensor = sensor;
            _onliThis = onliThis;
        }

        public override void startSomething()
        {

            if (!_wasDetected)
            {

                if (!nur_following)
                {
                    //send something to server
                    /*Event e = new Event();
                    e.sender = "kinect";
                    e.time = DateTime.Now;
                    e.message = "LeftHandToHead";
                    _client.SendEvent(e);*/
                    Console.WriteLine("Send Event LeftHandToHead - Only this skeleton");
                    //onlyThisSkeleton = me;
                    {
                        Thread thread = new Thread(startUpKinect_k);
                        thread.Start();
                    }
                }
            }
        }

        private void startUpKinect_k()
        {
            if (KinectSensor.KinectSensors.Count > 0)
            {
                _sensor = KinectSensor.KinectSensors[0];
            }

            if (_sensor.Status == KinectStatus.Connected)
            {
                _sensor.ColorStream.Enable();
                _sensor.DepthStream.Enable();
                _sensor.SkeletonStream.Enable();

                //only for Kinect for Microsoft version
                //sensor.DepthStream.Range = DepthRange.Near;
                //sensor.SkeletonStream.EnableTrackingInNearRange = true;
                //sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated
                _sensor.AllFramesReady += getMasterSkeleton;
                _sensor.Start();
            }
        }

        private void getMasterSkeleton(object sender, AllFramesReadyEventArgs e)
        {
            if (true)
            {
                Skeleton me = onliThis;
                getSkeleton(e, ref me);
                onliThis = me;
            }
        }

        public void getSkeleton(AllFramesReadyEventArgs e, ref Skeleton me)
        {
            using (SkeletonFrame skeletonData = e.OpenSkeletonFrame())
            {
                if (skeletonData == null)
                {
                    return;
                }

                skeletonData.CopySkeletonDataTo(skeleton);

                me = (from s in skeleton where s.TrackingState == SkeletonTrackingState.Tracked select s).FirstOrDefault();

            }
        }

        public override void stopSomething()
        {
            if (_wasDetected)
            {
                //send something to server
            }

        }
    }
}
