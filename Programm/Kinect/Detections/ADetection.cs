﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BotAtHWR;
using Microsoft.Kinect;

namespace kinect.Detections
{

    public abstract class ADetection
    {

        public JointType _type1;
        public JointType _type2;
        public static Skeleton onliThis;
        public static bool nur_following = false;
        public bool _wasDetected;

        public bool getDetection(ref Skeleton me, DepthImageFrame depthFrame)
        {
            DepthImagePoint _type1DepthPoint = depthFrame.MapFromSkeletonPoint(me.Joints[_type1].Position);
            ColorImagePoint _type1ColorPoint = depthFrame.MapToColorImagePoint(_type1DepthPoint.X, _type1DepthPoint.Y, ColorImageFormat.RgbResolution640x480Fps30);

            DepthImagePoint _type2DepthPoint = depthFrame.MapFromSkeletonPoint(me.Joints[_type2].Position);
            ColorImagePoint _type2ColorPoint = depthFrame.MapToColorImagePoint(_type2DepthPoint.X, _type2DepthPoint.Y, ColorImageFormat.RgbResolution640x480Fps30);

            double x = _type1ColorPoint.X - _type2ColorPoint.X;
            double y = _type1ColorPoint.Y - _type2ColorPoint.Y;
            double betrag = System.Math.Sqrt(x * x + y * y);

            if (betrag < 30)
            {
                startSomething();
                _wasDetected = true;
                return true;
            }
            else
            {
                stopSomething();
                _wasDetected = false;
                return false;
            }

        }

        public abstract void startSomething();
        public abstract void stopSomething();
    }
}
