﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;
using kinect.Detections;
using System.Threading;
namespace BotAtHWR
{
    public class Kinect
    {
        private List<ADetection> detections = new List<ADetection>();

        private KinectSensor sensor;

        private const int _skeletonCount = 6;

        private Skeleton[] allSkeletons = new Skeleton[_skeletonCount];

        //private Client _client;

        public Engine _engine;

        public Skeleton onlyThis;

        public Kinect(Engine engine)
        {
            _engine = engine;
        }


        public string start()
        {

            //_client = new Client("kinect", _logger);
            detections.Add(new LeftHandToLeftShoulder(_engine));
            detections.Add(new RightHandToRightShoulder(_engine));
            detections.Add(new RightHandToHipCenter(_engine));
            detections.Add(new LeftHandToRightShoulder(_engine));
            detections.Add(new LeftHandToFootLeft(_engine));
            detections.Add(new LeftHandToHipCenter(_engine));
            //PositionTracker.setClient(_engine);
            Thread thread = new Thread(startUpKinect);
            thread.Start();
            Thread.Sleep(4000);
            while(this.sensor.Status != KinectStatus.Connected)
            if (this.sensor.Status == KinectStatus.Connected) Console.WriteLine("hier sensor connected1");
            if (this.sensor.Status != KinectStatus.Connected) Console.WriteLine("hier sensor nicht connected1");
            detections.Add(new RightHandToHead(_engine, this.sensor));
            detections.Add(new LeftHandToHead(_engine, this.sensor, this.onlyThis));
            return "";
        }

        private void startUpKinect()
        {
            if (KinectSensor.KinectSensors.Count > 0)
            {
                sensor = KinectSensor.KinectSensors[0];
            }

            if (sensor.Status == KinectStatus.Connected)
            {
                sensor.ColorStream.Enable();
                sensor.DepthStream.Enable();
                sensor.SkeletonStream.Enable();

                //only for Kinect for Microsoft version
                //sensor.DepthStream.Range = DepthRange.Near;
                //sensor.SkeletonStream.EnableTrackingInNearRange = true;
                //sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;


                sensor.AllFramesReady += Sensor_AllFramesReady;
                sensor.Start();

            }
        }


        private void Sensor_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {

            Skeleton me = null;
            getSkeleton(e, ref me);
            if (me == null)
            {
            return;
            }
            detectGesture(e, ref me);
        }


        public void getSkeleton(AllFramesReadyEventArgs e, ref Skeleton me)
        {
            using (SkeletonFrame skeletonData = e.OpenSkeletonFrame())
            {
                if (skeletonData == null)
                {
                    return;
                }

                skeletonData.CopySkeletonDataTo(allSkeletons);

                me = (from s in allSkeletons where s.TrackingState == SkeletonTrackingState.Tracked select s).FirstOrDefault();

            }
        }

        private void detectGesture(AllFramesReadyEventArgs e, ref Skeleton me)
        {
            using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
            {
                PositionTracker.trackPos(ref me, depthFrame);
                if (depthFrame == null || sensor == null)
                {
                    return;
                }
                foreach (ADetection detection in detections)
                {
                    detection.getDetection(ref me, depthFrame);
                }
            }
        }
    }
}
