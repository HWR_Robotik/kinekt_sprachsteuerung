﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EposCmd.Net;
using EposCmd.Net.DeviceCmdSet.Operation;
using System.Timers;
using System.Threading;
using BotAtHWR;

namespace BotAtHWR
{
    /// <summary>
    /// Responsible for movement
    /// </summary>
    public class Engine
    {
        private DeviceManager _mConnector;

        //Two engines
        private Device epos1;
        private Device epos2;

        //Two modes each engine
        private ProfilePositionMode ppm1;
        private ProfilePositionMode ppm2;
        private ProfileVelocityMode pvm1;
        private ProfileVelocityMode pvm2;

        //Maximal velocity for every engine
        private const int MAX_VELOCITY = 12000000 - 1;

        private System.Timers.Timer timer;
        private uint profileDeceleration;
        private uint profileAcceleration;



        public Engine(int _profileAcceleration, int _profileDeceleration)
        {
            try
            {
                _mConnector = new DeviceManager("EPOS2", "MAXON SERIAL V2", "USB", "USB0");
                
                //get baudrate info
                uint b = _mConnector.Baudrate;

                //set connection properties
                _mConnector.Baudrate = 1000000;
                _mConnector.Timeout = 500;

            }
            catch (DeviceException e)
            {
                Console.WriteLine("Engine Exception d1: " + e.ErrorMessage, e.ErrorCode);
                
            }
            catch (Exception e)
            {

                Console.WriteLine("Engine Exception 1: " + e.Message);
            }

            if (_profileAcceleration >= 0 && _profileDeceleration >= 0)
            {
                ProfileAcceleration = (uint)_profileAcceleration;
                ProfileDeceleration = (uint)_profileDeceleration;
            }
            else
            {
                ProfileAcceleration = 0;
                ProfileDeceleration = 0;
            }

        }

        public uint ProfileAcceleration
        {
            get { return profileAcceleration; }
            set { profileAcceleration = value; }
        }


        public uint ProfileDeceleration
        {
            get { return profileDeceleration; }
            set { profileDeceleration = value; }
        }

        public int CurrentPosition
        {
            get { return epos1.Operation.MotionInfo.GetPositionIs(); }
        }

        public int CurrentVelocity
        {
            get { return epos1.Operation.MotionInfo.GetVelocityIs(); }
        }


        public bool Enable()
        {
            bool result = false;

            try
            {
                epos1 = _mConnector.CreateDevice(Convert.ToUInt16(1));
                epos2 = _mConnector.CreateDevice(Convert.ToUInt16(2));
                ppm1 = epos1.Operation.ProfilePositionMode;
                ppm2 = epos2.Operation.ProfilePositionMode;
                pvm1 = epos1.Operation.ProfileVelocityMode;
                pvm2 = epos2.Operation.ProfileVelocityMode;

                StateMachine sm = epos1.Operation.StateMachine;
                StateMachine sm2 = epos2.Operation.StateMachine;

                if (sm.GetFaultState())
                {
                    sm.ClearFault();
                }

                sm.SetEnableState();

                if (sm2.GetFaultState())
                {
                    sm2.ClearFault();
                }

                sm2.SetEnableState();

                result = true;
            }
            catch
            {
                result = false;
            }

            return result;
        }

        public void Disable()
        {
            StopWheels();

            try
            {
                StateMachine sm = epos1.Operation.StateMachine;
                StateMachine sm2 = epos2.Operation.StateMachine;

                if (sm.GetFaultState())
                {
                    sm.ClearFault();
                }

                if (sm2.GetFaultState())
                {
                    sm2.ClearFault();
                }

                if (!sm.GetDisableState())
                {
                    sm.SetDisableState();
                }

                if (!sm2.GetDisableState())
                {
                    sm2.SetDisableState();
                }

            }
            catch (DeviceException e)
            {
                Console.WriteLine("Engine Exception d2: "+ e.ErrorMessage, e.ErrorCode);
            }
            catch (Exception e)
            {

                Console.WriteLine("Engine Exception 2: " + e.Message);
            }
        }

        /// <summary>
        /// Stops the wheels immediately.
        /// </summary>
        public void StopImmediately()
        {
            try
            {
                ppm1.ActivateProfilePositionMode();
                ppm2.ActivateProfilePositionMode();

                ppm1.HaltPositionMovement();
                ppm2.HaltPositionMovement();
            }
            catch (DeviceException e)
            {
                Console.WriteLine("Engine Exception d3: " + e.ErrorMessage, e.ErrorCode);
            }
            catch (Exception e)
            {

                Console.WriteLine("Engine Exception 3: " + e.Message);
            }
        }

        /// <summary>
        /// Let the wheels turn in centimeter.
        /// </summary>
        /// <param name="distance">distance to move in cm</param>
        public void MoveInCm(int distance)
        { 
            bool arrived = false;

            try
            {
                ppm1.ActivateProfilePositionMode();
                ppm2.ActivateProfilePositionMode();

                ppm1.SetPositionProfile(1000000, 3000, 3000);
                ppm2.SetPositionProfile(1000000, 3000, 3000);
                ppm1.MoveToPosition(distance * -2000, false, true);
                ppm2.MoveToPosition(distance * -2000, false, true);
            }
            catch (DeviceException e)
            {
                Console.WriteLine("Engine Exception d4: " + e.ErrorMessage, e.ErrorCode);
            }
            catch (Exception e)
            {

                Console.WriteLine("Engine Exception 4: " + e.Message);
            }

            while (!arrived)
            {
                epos1.Operation.MotionInfo.GetMovementState(ref arrived);
            }
        }

        /// <summary>
        /// Let the wheels turn with the passed velocities.
        /// </summary>
        /// <param name="v1">Velocity left wheel</param>
        /// <param name="v2">Velocity right wheel</param>
        public void Move(Int32 v1, Int32 v2)
        {
            v1 = v1 * -1;
            v2 = v2 * -1;

            try
            {
                pvm1.ActivateProfileVelocityMode();
                pvm2.ActivateProfileVelocityMode();

                pvm1.SetVelocityProfile(ProfileAcceleration, ProfileDeceleration);
                pvm2.SetVelocityProfile(ProfileAcceleration, ProfileDeceleration);

                pvm1.MoveWithVelocity(v1);
                pvm2.MoveWithVelocity(v2);
            }
            catch (DeviceException e)
            {
                Console.WriteLine("Engine Exception d5: " + e.ErrorMessage, e.ErrorCode);
            }
            catch (Exception e)
            {
                Console.WriteLine("Engine Exception 5: " + e.Message);
            }
        }

        /// <summary>
        /// Wheels slow down until they stop
        /// </summary>
        public void StopWheels()
        {
            try
            {
                pvm1.ActivateProfileVelocityMode();
                pvm2.ActivateProfileVelocityMode();

                pvm1.SetVelocityProfile(ProfileAcceleration, ProfileDeceleration);
                pvm2.SetVelocityProfile(ProfileAcceleration, ProfileDeceleration);

                pvm1.MoveWithVelocity(0);
                pvm2.MoveWithVelocity(0);
            }
            catch (DeviceException e)
            {
                Console.WriteLine("Engine Exception d6: " + e.ErrorMessage, e.ErrorCode);
            }
            catch (Exception e)
            {
                Console.WriteLine("Engine Exception 6: " + e.Message);
            }
        }

        /// <summary>
        /// Wheels accelerate till passed speed and slow down again.
        /// The process needs much space, so be careful.
        /// </summary>
        /// <param name="v1">Velocity left wheel</param>
        /// <param name="v2">Velocity right wheel</param>
        public void ShowAcceleration(double v1, double v2)
        {
            Int32 vel1 = (int)Math.Round((v1 * 1000000 * -1), 0);
            Int32 vel2 = (int)Math.Round((v2 * 1000000 * -1), 0);

            if (vel1 > MAX_VELOCITY)
            {
                vel1 = MAX_VELOCITY;
            }
            else if (vel1 < -MAX_VELOCITY)
            {
                vel1 = -MAX_VELOCITY;
            }

            if (vel2 > MAX_VELOCITY)
            {
                vel2 = MAX_VELOCITY;
            }
            else if (vel2 < -MAX_VELOCITY)
            {
                vel2 = -MAX_VELOCITY;
            }

            try
            {
                timer = new System.Timers.Timer(8000);
                timer.Elapsed += new ElapsedEventHandler(StopsWheels);
                timer.Interval = 8000;
                timer.Enabled = true;

                pvm1.ActivateProfileVelocityMode();
                pvm2.ActivateProfileVelocityMode();

                pvm1.SetVelocityProfile(1000, 1000);
                pvm2.SetVelocityProfile(1000, 1000);

                pvm1.MoveWithVelocity(vel1);
                pvm2.MoveWithVelocity(vel2);

            }
            catch (DeviceException e)
            {
                Console.WriteLine("Engine Exception d7: " + e.ErrorMessage, e.ErrorCode);
            }
            catch (Exception e)
            {

                Console.WriteLine("Engine Exception 7: " + e.Message);
            }
        }

        private void StopsWheels(object source, ElapsedEventArgs e)
        {
            Move(0, 0);
            timer.Stop();
        }

        /* --------------------------------------------------------------------------------------------------
         * 
         * Ask Katja and Sidney whether we need this
         * 
         * 
        /// <summary>
        /// Wheels turn with the parameter Velocity
        /// </summary>
        /// <param name="v1">Velocity left wheel</param>
        /// <param name="v2">Velocity right wheel</param>
        public void Movetime(double v1, double v2)
        {
            try
            {
                if (storedVelocity ==  -50)
                {
                    double storedVelocity1 = (double)Math.Round((float)(_mEpos.Operation.ProfileVelocityMode.GetTargetVelocity() / 500000), 0);
                    double storedVelocity2 = (double)Math.Round((float)(_mEpos2.Operation.ProfileVelocityMode.GetTargetVelocity() / 500000), 0);

                    if (storedVelocity1 == storedVelocity2)
                    {
                        storedVelocity = storedVelocity1;
                    }

                    pvm1.ActivateProfileVelocityMode();
                    pvm2.ActivateProfileVelocityMode();

                    pvm1.SetVelocityProfile(15000, 15000);
                    pvm2.SetVelocityProfile(15000, 15000);

                    Int32 vel1 = (int)Math.Round((storedVelocity + v1 * 1000000 * -1), 0);
                    Int32 vel2 = (int)Math.Round((storedVelocity + v2 * 1000000 * -1), 0);
                    if (vel1 > MAX_VELOCITY) { vel1 = MAX_VELOCITY; }
                    if (vel1 < -MAX_VELOCITY) { vel1 = -MAX_VELOCITY; }
                    if (vel2 > MAX_VELOCITY) { vel2 = MAX_VELOCITY; }
                    if (vel2 < -MAX_VELOCITY) { vel2 = -MAX_VELOCITY; }

                    timer = new System.Timers.Timer(500);
                    timer.Elapsed += new ElapsedEventHandler(MoveWithPreviousVelocity);
                    timer.Interval = 500;
                    timer.Enabled = true;

                    pvm1.MoveWithVelocity(vel1);
                    pvm2.MoveWithVelocity(vel2);
                }
            }
            catch (DeviceException e)
            {
                Console.WriteLine(e.ErrorMessage, e.ErrorCode);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void MoveWithPreviousVelocity(object sender, ElapsedEventArgs e)
        {
            timer.Stop();

            pvm1.ActivateProfileVelocityMode();
            pvm2.ActivateProfileVelocityMode();

            pvm1.SetVelocityProfile(15000, 3000);
            pvm2.SetVelocityProfile(15000, 3000);

            Int32 vel1 = (int)Math.Round(storedVelocity * 500000, 0);
            Int32 vel2 = (int)Math.Round(storedVelocity * 500000, 0);
            if (vel1 > MAX_VELOCITY) { vel1 = MAX_VELOCITY; }
            if (vel1 < -MAX_VELOCITY) { vel1 = -MAX_VELOCITY; }
            if (vel2 > MAX_VELOCITY) { vel2 = MAX_VELOCITY; }
            if (vel2 < -MAX_VELOCITY) { vel2 = -MAX_VELOCITY; }
            pvm1.MoveWithVelocity(vel1);
            pvm2.MoveWithVelocity(vel2);
            storedVelocity = -50;
        }
         * 
         *----------------------------------------------------------------------------------------------------*/

        /// <summary>
        /// Let the wheels turn in opposite direction. The robot rotates by passed degrees.
        /// </summary>
        /// <param name="degree">degrees that the robot schould turn</param>
        public void TurnInDegrees(int degree)
        {
            bool arrived = false;

            try
            {
                ppm1.ActivateProfilePositionMode();
                ppm2.ActivateProfilePositionMode();
                if (degree > 0) {                 
                ppm1.MoveToPosition(degree * 700, false, true);
                ppm2.MoveToPosition(degree * -700, false, true);
                }
                if (degree < 0) {
                    degree = degree * (-1);
                ppm1.MoveToPosition(degree * -700, false, true);
                ppm2.MoveToPosition(degree * 700, false, true);
                }
            }
            catch (DeviceException e)
            {
                Console.WriteLine("Engine Exception d8: " + e.ErrorMessage, e.ErrorCode);
            }
            catch (Exception e)
            {

                Console.WriteLine("Engine Exception 8: " + e.Message);
            }

            while (!arrived)
            {
                epos1.Operation.MotionInfo.GetMovementState(ref arrived);
            }
        }
    }
}
